https://community.grafana.com/t/grafana-can-t-see-es-indexes/30463

## Services
Elasticsearch: localhost:9200

Grafana: localhost:3000

Seeder: Runs a powershell command to add some data to elasticsearch

## Instructions
```
docker-compose build
docker-compose up
```
Browse to localhost:3000 and login with the default grafana account: admin/admin

Add datasource elasticsearch (grafana runs in a docker itself, so we need to reference the elasticsearch docker instead of localhost)
* URL: http://elasticsearch:9200
* Index name: recipes
* Time field name: timestamp
* Version: 7.0+
* Save & Test

Created a new dashboard and add a panel with the elasticsearch datasource