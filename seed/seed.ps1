Write-Host "Wait for elasticsearch to start";
Start-Sleep -Seconds 30

Function StringDateToElasticDate([string]$inputDate) {
    (Get-Date $inputDate).ToUniversalTime().ToString("yyyy-MM-dd'T'HH:mm:ss.fffZ")
}

$randomObject = @{
    "timestamp"=StringDateToElasticDate (Get-Date).ToString()
    "title"="Another commit"
    "submitter"="-"
    "description"="-"
    "calories"=0
    "ingredients"=""
}

$elasticUri = "http://elasticsearch:9200/recipes/_doc"
Invoke-WebRequest -Headers @{"Content-Type" = "application/json" } -Uri $elasticUri -Body ($randomObject | ConvertTo-Json) -Method Post -UseBasicParsing